"use strict";

// let a = 2,
//     b = 4;
//     let result = (a, b) => a + b;
// console.log(result());


// let a = 2,
//     b = 4;
// let result = (a = 2, b = 4) => { // когда есть фигурные скобки в стрелочных функциях - нужен return.
//     return a + b;
// };
// console.log(result(3, 5));


// const user = {
//     name: "Vasya",
//     getName: function () {// Нужен function без стрелочной, иначе не выводит ничего
//         console.log(this.name); // Стрелочная функция не пропускает слово this.
//     },
// };
//
// user.getName();


// let a = 2;
// let b = 4;
// const getSum = (a, b) => a + b;
// console.log(getSum(a, b))


// const getSumm = (a, ...rest) => {
//     console.log(a);
//     console.log(rest);
// };
//
// getSumm(3, 4, 5, 6);


// const getSumm = (...rest) => {
//     let summ = 0;
//     for (let index = 0; index < rest.length; index++) {
//         summ += rest[index];
//     }
//     return summ;
// };
// console.log(getSumm(3, 4, 5, 6));


// const getMax = (...rest) => {
//     if (rest.length < 2) {
//         return `Error`;
//     }
//     for (let index = 0; index < rest.length; index++) {
//         if (isNaN(+rest[index]) || typeof rest[index] !== `number`) {
//             return `Error in ${index+1} argument`
//         }
//     }
//     return Math.max(...rest);
// };
// console.log(getMax(4, 6, "2"))


// const log = (message = "Внимание! Сообщение не указано.", count = 1) => {
//     for (let i = 0; i < count; i++) {
//         console.log(message);
//     }
// };
// log("Text", 5);


