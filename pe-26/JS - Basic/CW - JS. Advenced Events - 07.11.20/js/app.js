// // task 0
//
// const incrBtn = document.querySelector('.item-btn-incr');
// const decrBtn = document.querySelector('.item-btn-decr');
// const counter = document.querySelector('.item-counter');
// const changeCounter = (e) => {
//     // const counterValue = +counter.textContent; - строка на элемент след.строки counterValue + 1;
//     counter.textContent = counterValue + 1;
// };
//
// incrBtn.addEventListener('click', changeCounter);



// // task 1
//
// const container = document.createElement('div');
// const text = document.createElement('p');
//
// let htmlCode = '<button>1</button>\n<button>2</button>\n<button>3</button>\n<button>4</button>\n<button>5</button>';
// container.prepend(text);
//
// container.insertAdjacentHTML("afterbegin", htmlCode);
//
// container.addEventListener('click', ev => {
//     let content = ev.target.textContent;
//     text.textContent = content;
// })
// document.body.prepend(container);



// // task 2
//
// const openModal = document.createElement('button');
// openModal.textContent = 'show open';
//
// openModal.addEventListener('click', ev => {
//     const divModalWrap = document.createElement('div');
//     divModalWrap.className = 'modal-wrapper';
//
// divModalWrap.insertAdjacentHTML('afterbegin', `<div class="modal">
//     <button class="modal-close">x</button>
//     <p class="modal-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut cumque dolor fugiat nam nihil nulla praesentium ratione rem totam voluptatem! Ducimus esse ipsa porro. Adipisci aliquid asperiores hic minus officiis quaerat veritatis voluptate. Eius, fugit!</p>
//     </div>
// </div>`);
//
// // чтобы закрытие  окна работало по крестику
// divModalWrap.addEventListener('click', modalEvent => {
//     if (modalEvent.target.classList.contains('modal-close')){
//         divModalWrap.remove();
//     } else if (modalEvent.target.classList.contains('modal-wrapper')){
//         divModalWrap.remove();
//     }
// });
//
// // чтобы закрытие окна работало еще по кнопке
// document.addEventListener('keyup', e => {
//     if (e.key.toLowerCase() === 'escape') {
//         divModalWrap.remove();
//     }
// });
//
// document.body.prepend(divModalWrap);
// });
//
// document.body.prepend(openModal);



// LESSON PART 2

// // task 1
//
// const showMsg = (msgText, time) => {
//     setTimeout(() => document.write(msgText), time);
// };
//
// showMsg('here is your beer', 2000);



// // task 2
//
// const showMsg = (msgText, time, number) => {
//     let interval = setInterval(() => {
//         if (number-- === 0){
//             clearInterval(interval)
//         } else {
//             document.write(msgText);
//         };
//      }, time);
// };
//
// showMsg('here is your beer', 1000, 5);



// task 3

// console.log(localStorage.getItem('userName'));
// const userName = localStorage.getItem('userName');
// if (userName === null) {
//     const name = prompt("Enter your name");
//     localStorage.setItem('Hello', localStorage.getItem('userName'));
//
// } else {
//     console.log('Hello', userName);
// };



// task 4

const mailStorage = [
    {
        subject: "Hello world",
        from: "gogidoe@somemail.nothing",
        to: "lolabola@ui.ux",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    },
    {
        subject: "How could you?!",
        from: "ladyboss@somemail.nothing",
        to: "ingeneer@nomail.here",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    },
    {
        subject: "Acces denied",
        from: "info@cornhub.com",
        to: "gogidoe@somemail.nothing",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    }
];

const createEmailElem = (emailObj) => {
    const emailItem = document.createElement('div');
    emailItem.className = "email-item";

    emailItem.insertAdjacentHTML('beforeend', `
    <h4 class="email-subject">${emailObj.subject}</h4>
    
<div class="email-subject-wrapper">
    <a href="mailto:gogi@go.com" class="email-subtext">${emailObj.from}</a>
    <a href="mailto:gogi-double@go.com" class="email-subtext">${emailObj.to}</a>
</div>

<p class="email-text" hidden>${emailObj.text}</p>
    `);

    return emailItem;
};

const emailsContainer = document.querySelector('.emails');
const fragment = document.createDocumentFragment();

mailStorage.forEach((singleEmail) => {
    const emailElement = createEmailElem(singleEmail);
    fragment.append(emailElement);
});

// setTimeout(() => {
//     emailsContainer.append(fragment).remove();
// }, 2000);

emailsContainer.append(fragment);