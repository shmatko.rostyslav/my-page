// const user = {
//     name: 'Eran';
//     age: 13;
//     pets: [
//         {name: 'Ricci', type: 'squirrel', age: 2, status: 'alive'},
//         {name: 'Vic', type: 'worm', age: 0.8, status: 'lost'}
//         {name: 'John', type: 'dog', age: 8, status: 'dead'}
//     ]
// };
//
// console.log('users name - ', user.name);
// console.log('users age - ', user["age"]);
// console.log('users pets - ', user[propertyPets]);
// console.log('users first pet - ', user.pets[0]);


// function createUser(userName, userAge) {
//     const user = {
//         name: "userName",
//         age: "userAge"
//     };
//
//     return user;
// }
//
// const newUser = createUser(userName: "Serhio", userAge: 33);
// console.log(newUser);
//
// newUser.age++;
//
// console.log(newUser);


//
// function createUser(name, age) {
//     return {
//         name,
//         age
//     };
// }


// function createUser(userName, userAge) {
//     return {
//         name: userName,
//         age: userAge,
//         increaseAge: function () {
//             this.age++;
//         }
//     };
// }
//
// const user = createUser(userName: 'Gogi', userAge: 33);
// console.log(user);
// user.increaseAge();
// console.log(user);


// function createUser(userName, userAge) {
//     return {
//         name: userName,
//         age: userAge,
//         increaseAge: function () {
//             this.age++;
//         },
//         addField: function (fielldName, fieldValue) {
//             this[fielldName] = fieldValue
//         }
//     };
// }
// const newUser = createUser("Fray", 23);
// newUser.addField("pets", null);
// console.log(newUser);


// function User(userName, userAge) {
//     this.name = userName;
//     this.age = userAge;
//     this.increaseAge = function () {
//         this.age++;
//     };
//     this.addField = function (fieldName, fieldValue) {
//         this[fieldName] = fieldValue
//     }
// }
//
// debugger
// const user = new User("Ivan", 35);
// console.log(user);


const constantValues = {
    SIZE_SMALL: {
        price: 15,
        cal: 250,
    },

    SIZE_LARGE: {
        price: 25,
        cal: 340,
    },

    STUFFING_CHEASE: {
        price: 4,
        cal: 25,
    },

    STUFFING_SALAD: {
        price: 5,
        cal: 5,
    },

    STUFFING_BEEF: {
        price: 10,
        cal: 50,
    },
};

function Burger(size, stuffing) {
    this.size = constantValues[size];
    this.stuffing = constantValues[stuffing];

    this.calcPrice = function () {
        this.price = this.size.price + this.stuffing.price;
    };
    this.calcCalories = function () {
        this.cal = this.size.cal + this.stuffing.cal;
    };
}

const myBurger = new Burger("SIZE_LARGE", "STUFFING_BEEF");

myBurger.calcPrice();
console.log(myBurger.price);
myBurger.calcCalories();
console.log(myBurger.cal);

