// //task 1
// const func = () => {
//     let dish;
//     const allDishes = []
//     while (dish !== 'end') {
//     dish = prompt("Enter name of a dish");
//     allDishes.push(dish)
//     }
//     return allDishes
// }
//
// console.log(func());


// // task 2
// const getDishes = () => {
//     let dishesArr = [];
//     let dish;
//
//     do {
//         dish = prompt("Enter a dish");
//         if (dish!== "end") {
//             dishesArr.push(dish);
//         }
//     } while (dish !== "end");
// };
//
// const showDishes = (arr) => {
//     let arrLength = arr.length;
//     for (let i = 0; i < arr.length; i++) {
//         console.log(arr.shift());
//     }
//     console.log(arr);
// };
//
// showDishes(1, 2, 4, 6, 9);


// // task 3
// const copyArr = (arr) => {
//     return arr.map(addValue => addValue);
// };
//
// const copyArr = (arr) => {
//     return [...arr];
// };
//
// let numbers = [1, 2, 3, 4, 5];
// let clonedNumbers = copyArr(numbers);
//
// console.log(numbers === clonedNumbers);
// console.log(clonedNumbers);


// task 4


// // task 5
// const storage = ['apple', 'cheese', 'eggs', 'milk', 'grapes', 'sugar'];
//
// const replaceItems = (insteadOf, insertValue) => {
//     let replaceIndex = storage.indexOf(insteadOf);
//     let insertValues = insertValue.split(', ');
//     let uniqueInsertValues = new Set(insertValues);
//     storage.splice(replaceIndex, 1, ...uniqueInsertValues);
//     };
//
// replaceItems('grapes', 'wine, beer, wine, whiskey')
// console.log(storage);


// task 6

const USER_LIST = [
    {
        fullName: 'Gogi Doe',
        birthDate: '22/02/1998',
        pets: [
            {
                name: 'Doggidog',
                type: 'dog',
                status: 'alive'
            }
        ]
    },
    {
        fullName: 'Gogi1 Doe1',
        birthDate: '20/12/1996',
        pets: [
            {
                name: 'Cattycat',
                type: 'cat',
                status: 'alive'
            }
        ]
    },
    {
        fullName: 'Mister Nowho',
        birthDate: '20/12/1988',
        pets: [
            {
                name: 'Cattycat',
                type: 'iguana',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'Missis Nowho',
        birthDate: '20/08/1964',
        pets: [
            {
                name: 'Piter',
                type: 'spider',
                status: 'alive'
            }
        ]
    },
    {
        fullName: 'Helen Shmelen',
        birthDate: '17/08/1999',
        pets: [
            {
                name: 'Princess',
                type: 'peace of devil',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'John Doe',
        birthDate: '19/03/2002',
        pets: [
            {
                name: 'Lord',
                type: 'dog',
                status: 'alive'
            },
            {
                name: 'Gogi',
                type: 'koala',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'Georgina Doe',
        birthDate: '06/01/2000',
        pets: [
            {
                name: 'Koko',
                type: 'parrot',
                status: 'alive'
            },
            {
                name: 'Horhe',
                type: 'dog',
                status: 'alive'
            },
            {
                name: 'Piggy',
                type: 'pig',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'Nonamed Stranger',
        birthDate: '06/01/2000',
        pets: []
    },
    {
        fullName: 'Named Stranger',
        birthDate: '06/01/2000',
        pets: []
    },
];

const petLovers = (petLoversList) => {
    const catlovers = petLoversList.filter(user => {
        const isThereAnyCat = user.pets.some(pet => pet.type = 'cat');
        return isThereAnyCat
    });
    const doglovers = petLoversList.filter(user => {
        const isThereAnyDog = user.pets.some(pet => pet.type = 'dog');
        return isThereAnyDog;
    });
    const deadPets = petLoversList.filter(user => {
        const isThereAnyDeadPet = user.pets.some(pet => pet.status = 'dead');
        return isThereAnyDeadPet;
    });
    const noPetsAdults = petLoversList.filter(user => {
        const age = 2020 - new Date(user.birthDate).getFullYear();

        return age >= 18 && user.pet.length === 0;
    });
};