"use strict";

// let arr = []; //new Array(); - новый массив
// console.log(arr);
// console.log(typeof arr);


// let arr = [];
//
// arr.unshift(2); //добавляет элемент в начало массива
// arr.push(3); //добавляет элемент в конец массива
// arr.push(4);

// arr.shift(); //удаляет элемент в начале массива
// arr.pop(); //удаляет элемент в конце массива


console.log(arr);


for (let i = 0; i < arr.length; i++) {
    console.log(arr[i]);
}

const arrItemsIncrement = (value, i, array) => {
    array[i] = ++value;
};
arr.forEach(arrItemsIncrement); //callBackFn выполняется с каждой итерацией.
// Отправляется внутрь массива в качестве аргумента (круглые скобки не нужны)
// Выполняется заново с каждой итерацией (каждый раз)
// forEach перебирает значения, самостоятельно ничего не возвращает

console.log(arr);


// const arr2 = [{ id: 1 }, { id: 3 }, { id: 4 }, { id: 34 }];
//
// function compare(a, b) {
//     if (a < b) return 1;
//     if (a === b) return 0;
//     if (a > b) return -1;
// }
